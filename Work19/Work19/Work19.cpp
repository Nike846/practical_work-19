﻿#include <iostream>

class Animal
{
public:
    virtual void voice() = 0;
};

class Dog : public Animal
{
public:
    void voice() override
    {
        std::cout << "Woof!" << std::endl;
    }
};

class Cat : public Animal
{
public:
    void voice() override
    {
        std::cout << "Meow!" << std::endl;
    }
};

class Mouse : public Animal
{
public:
    void voice() override
    {
        std::cout << "Pi!" << std::endl;
    }
};


int main() {

    Dog* dog = new Dog;
    Cat* cat = new Cat;
    Mouse* mouse = new Mouse;

    Animal* animal[3]{ dog, cat, mouse };

    for (int i = 0; i < 3; ++i)
    {
        animal[i]->voice();
    }

    delete dog;
    delete cat;
    delete mouse;

    return 0;
}